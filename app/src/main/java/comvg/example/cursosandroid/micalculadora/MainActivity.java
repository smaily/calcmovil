package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText txtNum1;
    private EditText txtNum2;
    private EditText txtResult;
    private Button btnSumar, btnRestar, btnMulti, btnDiv, btnLimpiar, btnCerrar;
    Operaciones o = new Operaciones();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponts();
        limpiar();
    }

    public void initComponts(){
        txtNum1 = (EditText) findViewById(R.id.txtNum1);
        txtNum2 = (EditText) findViewById(R.id.txtNum2);
        txtResult = (EditText) findViewById(R.id.txtRes);

        btnSumar=(Button) findViewById(R.id.btnSuma);
        btnRestar=(Button) findViewById(R.id.btnResta);
        btnMulti=(Button) findViewById(R.id.btnMult);
        btnDiv=(Button) findViewById(R.id.btnDivi);
        btnLimpiar=(Button) findViewById(R.id.btnLimpiar);
        btnCerrar=(Button) findViewById(R.id.btnCerrar);
        setEventos();
    }
    public void setEventos(){
        this.btnSumar.setOnClickListener(this);
        this.btnRestar.setOnClickListener(this);
        this.btnMulti.setOnClickListener(this);
        this.btnDiv.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        float res=0;

        switch (view.getId()){
            case R.id.btnSuma: //suma
                valido(0);
                if(!(txtNum2.getText().toString().isEmpty()||txtNum1.getText().toString().isEmpty())){
                    res=o.suma();
                    txtResult.setText(String.valueOf(res));
                }
                break;

            case R.id.btnResta: //resta
                valido(0);
                if(!(txtNum2.getText().toString().isEmpty()||txtNum1.getText().toString().isEmpty())){
                    res=o.resta();
                    txtResult.setText(String.valueOf(res));
                }
                break;
            case R.id.btnMult: //Multiplicación
                valido(0);
                if(!(txtNum2.getText().toString().isEmpty()||txtNum1.getText().toString().isEmpty())){
                    res=o.mult();
                    txtResult.setText(String.valueOf(res));
                }
                break;
            case R.id.btnDivi: //División
                valido(1);
                if(!(txtNum2.getText().toString().isEmpty()||txtNum1.getText().toString().isEmpty())&&valido(1)){
                    res=o.div();
                    txtResult.setText(String.valueOf(res));
                }
                break;

            case R.id.btnLimpiar: //Limpiar
                limpiar();
                break;
            case R.id.btnCerrar: //Cerrar
                finish();
                break;
        }
    }

    public boolean valido(int c){
        boolean r=true;

        if(txtNum1.getText().toString().isEmpty()){
            txtNum1.setError("No puede estar vacío");
            txtNum1.requestFocus();
            txtResult.setText("");
        }
        else{
            o.setNum1(Float.parseFloat(txtNum1.getText().toString()));
        }
        if(txtNum2.getText().toString().isEmpty()){
            if(!(txtNum1.getText().toString().isEmpty())){
                txtNum2.requestFocus();
            }
            txtResult.setText("");
            txtNum2.setError("No puede estar vacío");
        }
        else{
            if(c==1){
                if(Integer.parseInt(txtNum2.getText().toString())==0){
                    txtNum2.setError("No se puede dividir entre 0");
                    txtNum2.requestFocus();
                    txtResult.setText("");
                    r=false;
                }
                else{
                    r=true;
                    o.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                }
            }
            else{
                o.setNum2(Float.parseFloat(txtNum2.getText().toString()));
            }
        }
        return r;
    }

    public void limpiar(){
        txtNum1.setText("");
        txtNum2.setText("");
        txtResult.setText("");
    }
}

